var listOfTags = ["google", "virtualbox", "redhat", "java", "vlabs", "webchat"];

var openHrefTagsInNewTab = function() {
    var atags = document.getElementsByTagName("a");
    var atagsArray = Array.from(atags);
    var filteredArray = atagsArray.filter(function(elem) {
    var retFlag = false;
    listOfTags.forEach(function(regEx) {
      if (elem.href.indexOf(regEx) > -1)
        retFlag = true;
    });
	return retFlag;
    });
    console.log(filteredArray.length);
    //filteredArray.map(function(elem) { return elem.target = "_blank"; });
    filteredArray.map(function(elem) { return elem.innerHTML = "Here is some text....."; });
}

Hooks.addHook('documentReady', openHrefTagsInNewTab);

$(document).ready(function () { 
  Hooks.runHooks('documentReady');
});
